import {Options} from 'react-native-navigation';

export enum ScreenName {
  WELCOME = 'WelcomeScreen',
  HOME = 'HomeScreen',
  DETAIL = 'DetailScreen',
  SAVED = 'SavedScreen',
}

interface NavigationProps {
  componentId: string;
}

interface NavigationOptions {
  options?: (passProps?: Record<string, any>) => Options | object;
}

export type NavigationComponent<P> = React.FC<P & NavigationProps> &
  NavigationOptions;

export type PassPropsType = {
  name?: string;
};

export interface AppRootState {
  hasOnboarded: boolean;
  passProps?: PassPropsType;
}
