import React, {useState, useEffect} from 'react';
import {View, StyleSheet, SafeAreaView, FlatList, Alert} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {useDispatch, useSelector} from 'react-redux';
import {NavigationComponent, ScreenName} from './types';
import {PodcastItem} from '../services/types';
import PodcastListItem from '../components/PodcastListItem';
import {resetApp, savePodcast} from '../store/actions';
import {startApp} from '../screens';
import {RootState} from '../store/reducers';
import PodcastSearchHeader from '../components/PodcastSearchHeader';
import usePodcastSearch from '../hooks/usePodcastSearch';

interface Props {}

const HomeScreen: NavigationComponent<Props> = ({componentId}) => {
  const [searchTerm, setSearchTerm] = useState('');
  const [loading, podcastList] = usePodcastSearch(searchTerm);
  const savedPodcastIds = useSelector((s: RootState) =>
    s.savedPodcasts.list.map((item) => item.collectionId),
  );
  const dispatch = useDispatch();

  const onSelection = (podcastItem: PodcastItem) => {
    dispatch(savePodcast(podcastItem));
    Alert.alert(
      'Saved podcast',
      `${podcastItem.collectionName} has been saved to your PodMarks 🤓`,
    );
  };

  useEffect(() => {
    const listener = Navigation.events().registerNavigationButtonPressedListener(
      (event) => {
        const buttonId = event.buttonId;

        switch (buttonId) {
          case 'signout':
            dispatch(resetApp());
            startApp({hasOnboarded: false});
            return;
          case 'saved':
            Navigation.showModal({
              stack: {
                children: [
                  {
                    component: {
                      name: ScreenName.SAVED,
                    },
                  },
                ],
              },
            });
            return;
        }
      },
    );

    return () => listener.remove();
  }, [componentId, dispatch]);

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <FlatList
          stickyHeaderIndices={[0]}
          ListHeaderComponent={
            <PodcastSearchHeader
              searchTerm={searchTerm}
              setSearchTerm={setSearchTerm}
              loading={loading}
            />
          }
          data={podcastList}
          keyExtractor={(item) => String(item.collectionId)}
          renderItem={({item}) => {
            const isSaved = savedPodcastIds.includes(item.collectionId);
            return (
              <PodcastListItem
                isSaved={isSaved}
                item={item}
                onSelection={isSaved ? () => {} : onSelection}
              />
            );
          }}
          showsVerticalScrollIndicator={false}
        />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#094f58',
    padding: 20,
    paddingBottom: 0,
    flex: 1,
  },
});

HomeScreen.options = (passProps) => ({
  topBar: {
    title: {
      text: `Welcome ${passProps?.name}`,
    },
    background: {
      translucent: true,
      color: 'transparent',
    },
    leftButtons: [
      {
        id: 'signout',
        text: 'Sign out',
        color: '#ffffff',
      },
    ],
    rightButtons: [
      {
        id: 'saved',
        text: 'Saved',
        color: '#ffffff',
      },
    ],
  },
});

export default HomeScreen;
