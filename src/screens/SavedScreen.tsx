import React, {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  FlatList,
  Alert,
} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {useSelector, useDispatch} from 'react-redux';
import {NavigationComponent} from './types';
import PodcastListItem from '../components/PodcastListItem';
import {PodcastItem, PodcastItemId} from '../services/types';
import {deletePodcast} from '../store/actions';
import {RootState} from '../store/reducers';

interface Props {}

const SavedScreen: NavigationComponent<Props> = ({componentId}) => {
  const podcastList = useSelector((s: RootState) => s.savedPodcasts.list);
  const dispatch = useDispatch();

  const onDeletePodcast = (podcastItemId: PodcastItemId) => {
    dispatch(deletePodcast(podcastItemId));
  };

  const showDeletionAlert = (item: PodcastItem) => {
    Alert.alert(
      'Delete podcast',
      `Are you sure you want to delete ${item.collectionName} from your saved list?`,
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'Confirm',
          onPress: () => onDeletePodcast(item.collectionId),
        },
      ],
    );
  };

  useEffect(() => {
    const listener = Navigation.events().registerNavigationButtonPressedListener(
      (event) => {
        const buttonId = event.buttonId;

        switch (buttonId) {
          case 'close':
            Navigation.dismissModal(componentId);
            return;
        }
      },
    );

    return () => listener.remove();
  }, [componentId]);

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <FlatList
          data={podcastList}
          keyExtractor={(item) => String(item.collectionId)}
          renderItem={({item}) => (
            <PodcastListItem
              isSaved
              item={item}
              onSelection={() => showDeletionAlert(item)}
            />
          )}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={
            <View style={styles.emptyView}>
              <Text style={styles.text}>
                You don't have any saved podcasts 😟
              </Text>
            </View>
          }
        />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#094f58',
    padding: 20,
    paddingBottom: 0,
    flex: 1,
  },
  emptyView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  text: {
    color: '#ffffff',
  },
});

SavedScreen.options = () => ({
  topBar: {
    title: {
      text: 'Saved Podcasts',
    },
    background: {
      translucent: true,
      color: 'transparent',
    },
    leftButtons: [
      {
        id: 'close',
        text: 'Close',
        color: '#ffffff',
      },
    ],
  },
});

export default SavedScreen;
