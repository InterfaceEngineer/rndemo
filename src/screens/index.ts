import {Navigation} from 'react-native-navigation';
import {ScreenName, AppRootState} from './types';
import WelcomeScreen from './WelcomeScreen';
import HomeScreen from './HomeScreen';
import SavedScreen from './SavedScreen';
import {withStore} from '../containers/withStore';

const Screens = new Map<ScreenName, React.FC<any>>();
Screens.set(ScreenName.WELCOME, WelcomeScreen);
Screens.set(ScreenName.HOME, HomeScreen);
Screens.set(ScreenName.SAVED, SavedScreen);

function registerScreens() {
  Screens.forEach((C, key) => {
    Navigation.registerComponent(
      key,
      () => withStore(C),
      () => C,
    );
  });
}

function startApp(state: AppRootState) {
  const {hasOnboarded, passProps} = state;

  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: hasOnboarded ? ScreenName.HOME : ScreenName.WELCOME,
              passProps,
            },
          },
        ],
      },
    },
  });
}

export {registerScreens, startApp};
