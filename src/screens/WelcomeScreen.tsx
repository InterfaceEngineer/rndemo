import React, {useState, useEffect} from 'react';
import {View, Text, TextInput, StyleSheet, SafeAreaView} from 'react-native';
import {Navigation, OptionsTopBarButton} from 'react-native-navigation';
import {useDispatch} from 'react-redux';
import {NavigationComponent} from './types';
import {setUser} from '../store/actions';
import {startApp} from '../screens';

const nextNavButton = (enabled: boolean): OptionsTopBarButton => ({
  id: 'next',
  text: 'Next',
  color: '#ffffff',
  enabled,
});

interface Props {}

const WelcomeScreen: NavigationComponent<Props> = ({componentId}) => {
  const [name, setName] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    Navigation.mergeOptions(componentId, {
      topBar: {
        rightButtons: [nextNavButton(name.length >= 2)],
      },
    });
  }, [componentId, name]);

  useEffect(() => {
    const listener = Navigation.events().registerNavigationButtonPressedListener(
      (event) => {
        const buttonId = event.buttonId;

        switch (buttonId) {
          case 'next':
            dispatch(setUser(name));
            startApp({hasOnboarded: true, passProps: {name}});
            return;
        }
      },
    );

    return () => listener.remove();
  }, [componentId, name, dispatch]);

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <Text style={[styles.text, styles.title]}>Hello there</Text>
        <Text style={[styles.text, styles.label]}>
          Welcome to PodMarkin. An app which allows you to search and save the
          podcasts you love into your own collection called PodMarkin. Enter
          your name to begin PodMarkin 🤓
        </Text>
        <TextInput
          style={styles.textInput}
          onChangeText={(text) => setName(text)}
          value={name}
          placeholder={'e.g. John Doe'}
          placeholderTextColor={'#14141433'}
        />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#d24c4a',
    flex: 1,
    paddingHorizontal: 20,
  },
  text: {
    color: '#ffffff',
  },
  title: {
    fontSize: 32,
    paddingVertical: 20,
  },
  label: {
    fontSize: 16,
    lineHeight: 22,
  },
  textInput: {
    height: 40,
    backgroundColor: '#ffffff',
    borderRadius: 6,
    color: '#141414',
    marginVertical: 20,
    paddingHorizontal: 10,
  },
});

WelcomeScreen.options = () => ({
  topBar: {
    title: {
      text: 'PodMarkin',
    },
    background: {
      translucent: true,
      color: 'transparent',
    },
    rightButtons: [nextNavButton(false)],
  },
});

export default WelcomeScreen;
