export type PodcastItemId = number;
type URL = string;
type ImageURL = string;
type Price = number;
type DateTimeZone = string; // e.g. "2020-08-31T19:15:00Z"
type Explicitness = 'cleaned' | 'explicit' | string;
type ContentAdvisory = 'Cleaned' | 'Explicit' | string;

export type PodcastItem = {
  wrapperType: string;
  kind: string;
  artistId: number;
  collectionId: PodcastItemId; // N.B. We will use this as our Primary Key
  trackId: number;
  artistName: string;
  collectionName: string;
  trackName: string;
  collectionCensoredName: string;
  trackCensoredName: string;
  artistViewUrl: URL;
  collectionViewUrl: URL;
  feedUrl: URL;
  trackViewUrl: URL;
  artworkUrl30: ImageURL;
  artworkUrl60: ImageURL;
  artworkUrl100: ImageURL;
  collectionPrice: Price;
  trackPrice: Price;
  trackRentalPrice: Price;
  collectionHdPrice: Price;
  trackHdPrice: Price;
  trackHdRentalPrice: Price;
  releaseDate: DateTimeZone;
  collectionExplicitness: Explicitness;
  trackExplicitness: Explicitness;
  trackCount: number;
  country: string;
  currency: string;
  primaryGenreName: string;
  contentAdvisoryRating: ContentAdvisory;
  artworkUrl600: ImageURL;
  genreIds: string[];
  genres: string[];
};

export type iTunesResponse = {
  resultCount: number;
  results: PodcastItem[];
};
