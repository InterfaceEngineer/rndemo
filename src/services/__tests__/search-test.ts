import SearchService from '../search';
import {iTunesStubSuccess, podcastItemStub} from '../__mocks__';

beforeEach(() => {
  global.fetch = jest.fn().mockImplementation(
    () =>
      new Promise((resolve) => {
        resolve({
          status: 200,
          json: () => iTunesStubSuccess,
        });
      }),
  );
});

describe('SearchService', () => {
  it('should fetch podcast results', async () => {
    const response = await SearchService('The Daily');
    expect(response!.length).toEqual(3);
    expect(response![0]).toEqual(podcastItemStub);
  });
});
