import {PodcastItem, iTunesResponse} from './types';
import api from '../config/api';

async function SearchService(term: string): Promise<PodcastItem[] | null> {
  const searchTerm = term.replace(/\s\s+/g, ' ').split(' ').join('+');

  try {
    const response = await fetch(`${api.endpoint}&term=${searchTerm}`);

    if (response.status >= 200 && response.status <= 299) {
      const json: iTunesResponse = await response.json();
      return json.results;
    }

    throw Error(response.statusText);
  } catch (error) {
    console.error(error);
    return null;
  }
}

export default SearchService;
