import {iTunesResponse, PodcastItem} from '../types';

const podcastItemStub: PodcastItem = {
  wrapperType: 'track',
  kind: 'podcast',
  artistId: 121664449,
  collectionId: 1200361736,
  trackId: 1200361736,
  artistName: 'The New York Times',
  collectionName: 'The Daily',
  trackName: 'The Daily',
  collectionCensoredName: 'The Daily',
  trackCensoredName: 'The Daily',
  artistViewUrl:
    'https://podcasts.apple.com/us/artist/the-new-york-times/121664449?uo=4',
  collectionViewUrl:
    'https://podcasts.apple.com/us/podcast/the-daily/id1200361736?uo=4',
  feedUrl: 'http://rss.art19.com/the-daily',
  trackViewUrl:
    'https://podcasts.apple.com/us/podcast/the-daily/id1200361736?uo=4',
  artworkUrl30:
    'https://is3-ssl.mzstatic.com/image/thumb/Podcasts114/v4/93/cf/99/93cf9960-fff3-205e-6655-72c03ddeccb2/mza_10545582232493904175.jpeg/30x30bb.jpg',
  artworkUrl60:
    'https://is3-ssl.mzstatic.com/image/thumb/Podcasts114/v4/93/cf/99/93cf9960-fff3-205e-6655-72c03ddeccb2/mza_10545582232493904175.jpeg/60x60bb.jpg',
  artworkUrl100:
    'https://is3-ssl.mzstatic.com/image/thumb/Podcasts114/v4/93/cf/99/93cf9960-fff3-205e-6655-72c03ddeccb2/mza_10545582232493904175.jpeg/100x100bb.jpg',
  collectionPrice: 0.0,
  trackPrice: 0.0,
  trackRentalPrice: 0,
  collectionHdPrice: 0,
  trackHdPrice: 0,
  trackHdRentalPrice: 0,
  releaseDate: '2020-09-04T10:06:00Z',
  collectionExplicitness: 'cleaned',
  trackExplicitness: 'cleaned',
  trackCount: 300,
  country: 'USA',
  currency: 'USD',
  primaryGenreName: 'Daily News',
  contentAdvisoryRating: 'Clean',
  artworkUrl600:
    'https://is3-ssl.mzstatic.com/image/thumb/Podcasts114/v4/93/cf/99/93cf9960-fff3-205e-6655-72c03ddeccb2/mza_10545582232493904175.jpeg/600x600bb.jpg',
  genreIds: ['1526', '26', '1489'],
  genres: ['Daily News', 'Podcasts', 'News'],
};

const iTunesStubSuccess: iTunesResponse = {
  resultCount: 3,
  results: [
    podcastItemStub,
    {
      wrapperType: 'track',
      kind: 'podcast',
      artistId: 1287316832,
      collectionId: 1047335260,
      trackId: 1047335260,
      artistName: 'The Daily Wire',
      collectionName: 'The Ben Shapiro Show',
      trackName: 'The Ben Shapiro Show',
      collectionCensoredName: 'The Ben Shapiro Show',
      trackCensoredName: 'The Ben Shapiro Show',
      artistViewUrl:
        'https://podcasts.apple.com/us/artist/the-daily-wire/1287316832?uo=4',
      collectionViewUrl:
        'https://podcasts.apple.com/us/podcast/the-ben-shapiro-show/id1047335260?uo=4',
      feedUrl: 'https://feeds.megaphone.fm/WWO8086402096',
      trackViewUrl:
        'https://podcasts.apple.com/us/podcast/the-ben-shapiro-show/id1047335260?uo=4',
      artworkUrl30:
        'https://is3-ssl.mzstatic.com/image/thumb/Podcasts113/v4/94/e9/63/94e9631c-887b-dd65-0487-62dde2bacaab/mza_13932341250045832525.jpg/30x30bb.jpg',
      artworkUrl60:
        'https://is3-ssl.mzstatic.com/image/thumb/Podcasts113/v4/94/e9/63/94e9631c-887b-dd65-0487-62dde2bacaab/mza_13932341250045832525.jpg/60x60bb.jpg',
      artworkUrl100:
        'https://is3-ssl.mzstatic.com/image/thumb/Podcasts113/v4/94/e9/63/94e9631c-887b-dd65-0487-62dde2bacaab/mza_13932341250045832525.jpg/100x100bb.jpg',
      collectionPrice: 0.0,
      trackPrice: 0.0,
      trackRentalPrice: 0,
      collectionHdPrice: 0,
      trackHdPrice: 0,
      trackHdRentalPrice: 0,
      releaseDate: '2020-09-06T12:00:00Z',
      collectionExplicitness: 'cleaned',
      trackExplicitness: 'cleaned',
      trackCount: 300,
      country: 'USA',
      currency: 'USD',
      primaryGenreName: 'News',
      contentAdvisoryRating: 'Clean',
      artworkUrl600:
        'https://is3-ssl.mzstatic.com/image/thumb/Podcasts113/v4/94/e9/63/94e9631c-887b-dd65-0487-62dde2bacaab/mza_13932341250045832525.jpg/600x600bb.jpg',
      genreIds: ['1489', '26'],
      genres: ['News', 'Podcasts'],
    },
    {
      wrapperType: 'track',
      kind: 'podcast',
      artistId: 204040224,
      collectionId: 160904630,
      trackId: 160904630,
      artistName: 'TED',
      collectionName: 'TED Talks Daily',
      trackName: 'TED Talks Daily',
      collectionCensoredName: 'TED Talks Daily',
      trackCensoredName: 'TED Talks Daily',
      artistViewUrl:
        'https://podcasts.apple.com/us/artist/ted-talks/204040224?uo=4',
      collectionViewUrl:
        'https://podcasts.apple.com/us/podcast/ted-talks-daily/id160904630?uo=4',
      feedUrl: 'http://feeds.feedburner.com/TEDTalks_audio',
      trackViewUrl:
        'https://podcasts.apple.com/us/podcast/ted-talks-daily/id160904630?uo=4',
      artworkUrl30:
        'https://is5-ssl.mzstatic.com/image/thumb/Podcasts124/v4/8c/da/c9/8cdac9f2-b488-a9c4-e611-2a677fa59ae2/mza_3718282663413307677.png/30x30bb.jpg',
      artworkUrl60:
        'https://is5-ssl.mzstatic.com/image/thumb/Podcasts124/v4/8c/da/c9/8cdac9f2-b488-a9c4-e611-2a677fa59ae2/mza_3718282663413307677.png/60x60bb.jpg',
      artworkUrl100:
        'https://is5-ssl.mzstatic.com/image/thumb/Podcasts124/v4/8c/da/c9/8cdac9f2-b488-a9c4-e611-2a677fa59ae2/mza_3718282663413307677.png/100x100bb.jpg',
      collectionPrice: 0.0,
      trackPrice: 0.0,
      trackRentalPrice: 0,
      collectionHdPrice: 0,
      trackHdPrice: 0,
      trackHdRentalPrice: 0,
      releaseDate: '2020-09-04T15:01:00Z',
      collectionExplicitness: 'cleaned',
      trackExplicitness: 'cleaned',
      trackCount: 234,
      country: 'USA',
      currency: 'USD',
      primaryGenreName: 'Education',
      contentAdvisoryRating: 'Clean',
      artworkUrl600:
        'https://is5-ssl.mzstatic.com/image/thumb/Podcasts124/v4/8c/da/c9/8cdac9f2-b488-a9c4-e611-2a677fa59ae2/mza_3718282663413307677.png/600x600bb.jpg',
      genreIds: ['1304', '26', '1324'],
      genres: ['Education', 'Podcasts', 'Society & Culture'],
    },
  ],
};

export {podcastItemStub, iTunesStubSuccess};
