import React from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Text,
  TextInput,
} from 'react-native';

interface Props {
  searchTerm: string;
  setSearchTerm: Function;
  loading: boolean;
}

const PodcastSearchHeader: React.FC<Props> = ({
  searchTerm,
  setSearchTerm,
  loading,
}) => (
  <View style={styles.searchContainer}>
    <Text style={styles.intro}>
      Search below for your favourite podcasts. You can type anything from parts
      of the podcast name to any of the hosts on the show...
    </Text>
    <View style={styles.searchView}>
      <TextInput
        style={styles.textInput}
        onChangeText={(text) => setSearchTerm(text)}
        value={searchTerm}
        placeholder={'e.g. The Daily'}
        placeholderTextColor={'#14141433'}
      />
      <ActivityIndicator
        style={styles.activityIndicator}
        size="small"
        color="#ffffff"
        animating={loading}
      />
    </View>
  </View>
);

export default PodcastSearchHeader;

const styles = StyleSheet.create({
  searchContainer: {
    backgroundColor: '#094f58',
  },
  searchView: {
    flexDirection: 'row',
  },
  textInput: {
    height: 40,
    backgroundColor: '#ffffff',
    borderRadius: 6,
    color: '#141414',
    flex: 2,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  activityIndicator: {
    width: 40,
  },
  intro: {
    color: '#ffffff',
    fontSize: 16,
    lineHeight: 22,
    paddingVertical: 10,
  },
});
