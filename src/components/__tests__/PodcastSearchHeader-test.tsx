import React from 'react';
import renderer from 'react-test-renderer';
import {shallow} from 'enzyme';
import PodcastSearchHeader from '../PodcastSearchHeader';

describe('PodcastSearchHeader', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <PodcastSearchHeader
          searchTerm={'The Daily'}
          setSearchTerm={jest.fn()}
          loading
        />,
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should have spinner animating', () => {
    const wrapper = shallow(
      <PodcastSearchHeader
        searchTerm={'The Daily'}
        setSearchTerm={jest.fn()}
        loading
      />,
    );

    const spinner = wrapper.find('ActivityIndicator');
    const expectedSpinnerProps = {
      style: {width: 40},
      size: 'small',
      color: '#ffffff',
      animating: true,
      hidesWhenStopped: true,
    };

    expect(spinner.length).toBe(1);
    expect(spinner.props()).toEqual(expectedSpinnerProps);
  });

  it('should have spinner not animating', () => {
    const wrapper = shallow(
      <PodcastSearchHeader
        searchTerm={'The Daily'}
        setSearchTerm={jest.fn()}
        loading={false}
      />,
    );

    const spinner = wrapper.find('ActivityIndicator');
    const expectedSpinnerProps = {
      style: {width: 40},
      size: 'small',
      color: '#ffffff',
      animating: false,
      hidesWhenStopped: true,
    };

    expect(spinner.length).toBe(1);
    expect(spinner.props()).toEqual(expectedSpinnerProps);
  });
});
