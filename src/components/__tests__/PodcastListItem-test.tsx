import React from 'react';
import renderer from 'react-test-renderer';
import {shallow} from 'enzyme';
import PodcastListItem from '../PodcastListItem';
import {podcastItemStub} from '../../services/__mocks__';

describe('PodcastListItem', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <PodcastListItem
          isSaved
          item={podcastItemStub}
          onSelection={jest.fn()}
        />,
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('displays saved icon', () => {
    const wrapper = shallow(
      <PodcastListItem
        isSaved
        item={podcastItemStub}
        onSelection={jest.fn()}
      />,
    );

    const savedIconNode = wrapper.findWhere(
      (node: {prop: (arg0: string) => string}) =>
        node.prop('testID') === 'saved-icon',
    );
    expect(savedIconNode.length).toBe(1);
  });

  it('displays without saved icon', () => {
    const wrapper = shallow(
      <PodcastListItem
        isSaved={false}
        item={podcastItemStub}
        onSelection={jest.fn()}
      />,
    );

    const savedIconNode = wrapper.findWhere(
      (node: {prop: (arg0: string) => string}) =>
        node.prop('testID') === 'saved-icon',
    );
    expect(savedIconNode.length).toBe(0);
  });
});
