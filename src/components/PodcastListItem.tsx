import React from 'react';
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import {PodcastItem} from '../services/types';

interface Props {
  item: PodcastItem;
  onSelection: (item: PodcastItem) => void;
  isSaved: boolean;
}

const PodcastListItem: React.FC<Props> = ({item, onSelection, isSaved}) => (
  <TouchableOpacity style={styles.container} onPress={() => onSelection(item)}>
    <>
      <Image source={{uri: item.artworkUrl100}} style={styles.image} />
      <View style={styles.content}>
        <Text style={[styles.text, styles.title]}>{item.collectionName}</Text>
        <Text style={[styles.text, styles.description]}>
          By {item.artistName}
        </Text>
        <Text style={[styles.text, styles.description]}>
          Genre: {item.primaryGenreName}
        </Text>
        <Text style={[styles.text, styles.description]}>
          {item.trackCount} episodes
        </Text>
      </View>
      {isSaved && (
        <View style={styles.savedIconContainer} testID="saved-icon">
          <Text>♥️</Text>
        </View>
      )}
    </>
  </TouchableOpacity>
);

export default PodcastListItem;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    alignItems: 'center',
    backgroundColor: '#ffffff33',
    width: '100%',
    flexDirection: 'row',
    borderRadius: 6,
  },
  content: {
    flexDirection: 'column',
    flex: 1,
    marginLeft: 10,
  },
  text: {
    color: '#ffffff99',
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 6,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  description: {
    fontSize: 12,
  },
  savedIconContainer: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
});
