import {ActionKeys, UserActionType, SavedPodcastActionType} from './types';
import {PodcastItem, PodcastItemId} from '../services/types';

export const setUser = (name: string): UserActionType => ({
  type: ActionKeys.SET_USER,
  payload: name,
});

export const resetApp = (): {type: ActionKeys} => ({
  type: ActionKeys.RESET,
});

export const savePodcast = (
  podcastItem: PodcastItem,
): SavedPodcastActionType => ({
  type: ActionKeys.SAVE_PODCAST,
  payload: podcastItem,
});

export const deletePodcast = (
  podcastItemId: PodcastItemId,
): SavedPodcastActionType => ({
  type: ActionKeys.DELETE_PODCAST,
  payload: podcastItemId,
});
