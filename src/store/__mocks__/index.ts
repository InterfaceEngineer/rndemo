import configureStore from 'redux-mock-store';
import {RootState} from '../reducers';

const mockStore = configureStore();

const initialState: RootState = {
  user: {
    name: null,
  },
  savedPodcasts: {
    list: [],
  },
};

const store = mockStore(initialState);

export {initialState, store};
