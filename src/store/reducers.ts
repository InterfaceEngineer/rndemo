import {
  ActionKeys,
  UserState,
  UserActionType,
  SavedPodcastsState,
  SavedPodcastActionType,
} from './types';
import {PodcastItem} from '../services/types';
import {combineReducers} from 'redux';

const initialUserState: UserState = {
  name: null,
};

const user = (
  state: UserState = initialUserState,
  action: UserActionType,
): UserState => {
  switch (action.type) {
    case ActionKeys.SET_USER:
      return {
        name: action.payload as string,
      };
    case ActionKeys.RESET:
      return initialUserState;
    default:
      return state;
  }
};

const initialSavedPodcastsState: SavedPodcastsState = {
  list: [],
};

const savedPodcasts = (
  state: SavedPodcastsState = initialSavedPodcastsState,
  action: SavedPodcastActionType,
): SavedPodcastsState => {
  switch (action.type) {
    case ActionKeys.SAVE_PODCAST:
      return {
        list: [...state.list, action.payload as PodcastItem],
      };
    case ActionKeys.DELETE_PODCAST:
      const newList = state.list.filter(
        (podcastItem) => podcastItem.collectionId !== action.payload,
      );

      return {
        ...state,
        list: newList,
      };
    case ActionKeys.RESET:
      return initialSavedPodcastsState;
    default:
      return state;
  }
};

export const rootReducer = combineReducers({
  user,
  savedPodcasts,
});

export type RootState = ReturnType<typeof rootReducer>;

export const reducerTestExports = {
  user,
  savedPodcasts,
};
