import {setUser, resetApp, savePodcast, deletePodcast} from '../actions';
import {store} from '../__mocks__';
import {podcastItemStub} from '../../services/__mocks__';

beforeEach(() => {
  store.clearActions();
});

describe('setUser', () => {
  it('should dispatch the correct action', () => {
    store.dispatch(setUser('John Doe'));

    const actions = store.getActions();
    const expectedPayload = {
      type: 'SET_USER',
      payload: 'John Doe',
    };

    expect(actions).toEqual([expectedPayload]);
  });
});

describe('resetApp', () => {
  it('should dispatch the correct action', () => {
    store.dispatch(resetApp());

    const actions = store.getActions();
    const expectedPayload = {
      type: 'RESET',
    };

    expect(actions).toEqual([expectedPayload]);
  });
});

describe('savePodcast', () => {
  it('should dispatch the correct action', () => {
    store.dispatch(savePodcast(podcastItemStub));

    const actions = store.getActions();
    const expectedPayload = {
      type: 'SAVE_PODCAST',
      payload: podcastItemStub,
    };

    expect(actions).toEqual([expectedPayload]);
  });
});

describe('deletePodcast', () => {
  it('should dispatch the correct action', () => {
    store.dispatch(deletePodcast(podcastItemStub.collectionId));

    const actions = store.getActions();
    const expectedPayload = {
      type: 'DELETE_PODCAST',
      payload: podcastItemStub.collectionId,
    };

    expect(actions).toEqual([expectedPayload]);
  });
});
