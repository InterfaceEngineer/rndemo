import {initialState} from '../__mocks__';
import {SavedPodcastActionType, ActionKeys} from '../types';
import {reducerTestExports} from '../reducers';
import {podcastItemStub} from '../../services/__mocks__';

const {savedPodcasts} = reducerTestExports;

describe('savedPodcasts reducer', () => {
  it('should save podcast item', () => {
    const action: SavedPodcastActionType = {
      type: ActionKeys.SAVE_PODCAST,
      payload: podcastItemStub,
    };

    const reducerState = savedPodcasts(initialState.savedPodcasts, action);

    expect(reducerState).toEqual({
      list: [podcastItemStub],
    });
  });

  it('should delete podcast item', () => {
    const action: SavedPodcastActionType = {
      type: ActionKeys.DELETE_PODCAST,
      payload: podcastItemStub.collectionId,
    };

    const reducerState = savedPodcasts({list: [podcastItemStub]}, action);

    expect(reducerState).toEqual({
      list: [],
    });
  });

  it('should revert back to empty list when global app reset is called', () => {
    const action: SavedPodcastActionType = {
      type: ActionKeys.RESET,
    };

    const reducerState = savedPodcasts({list: [podcastItemStub]}, action);

    expect(reducerState).toEqual({
      list: [],
    });
  });
});
