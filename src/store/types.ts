import {PodcastItem, PodcastItemId} from '../services/types';

export enum ActionKeys {
  SET_USER = 'SET_USER',
  RESET = 'RESET',
  SAVE_PODCAST = 'SAVE_PODCAST',
  DELETE_PODCAST = 'DELETE_PODCAST',
}

/* User */
interface UserAction {
  type: ActionKeys;
  payload?: string;
}
export type UserActionType = UserAction;
export type UserState = {
  name: string | null;
};

/* Podcasts */
interface SavePodcastAction {
  readonly type: ActionKeys;
  readonly payload?: PodcastItem;
}
interface DeletePodcastAction {
  readonly type: ActionKeys;
  readonly payload?: PodcastItemId;
}

export type SavedPodcastActionType = SavePodcastAction | DeletePodcastAction;

export type SavedPodcastsState = {
  list: PodcastItem[];
};
