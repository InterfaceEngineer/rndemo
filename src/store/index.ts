import AsyncStorage from '@react-native-community/async-storage';
import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import {createLogger} from 'redux-logger';
import {rootReducer} from './reducers';

/* Middleware */
const rootMiddleware = [];
if (__DEV__) {
  const logger = createLogger({
    collapsed: true,
    diff: true,
  });

  // @ts-ignore
  rootMiddleware.push(logger);
}

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReducer, applyMiddleware(...rootMiddleware));
// @ts-ignore - TODO: Await resolution from this issue: https://github.com/rt2zz/redux-persist/issues/1140
const persistor = persistStore(store);

export {store, persistor};
