import {useState, useEffect} from 'react';
import {PodcastItem} from '../services/types';
import SearchService from '../services/search';

function usePodcastSearch(searchTerm: string): [boolean, PodcastItem[]] {
  const [loading, setLoading] = useState(false);
  const [podcastList, setPodcastList] = useState<PodcastItem[]>([]);

  useEffect(() => {
    async function fetchPodcasts() {
      setLoading(true);
      const response = await SearchService(searchTerm);
      if (response) {
        setPodcastList(response);
      }
      setLoading(false);
    }

    if (searchTerm.length > 2) {
      fetchPodcasts();
    }
  }, [searchTerm]);

  return [loading, podcastList];
}

export default usePodcastSearch;
