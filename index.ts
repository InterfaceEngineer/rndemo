import {Navigation} from 'react-native-navigation';
import {registerScreens, startApp} from './src/screens';
import {AppRootState, PassPropsType} from './src/screens/types';
import {store} from './src/store';

registerScreens();
let unsubscribe: Function;

function onAppLaunch(state: AppRootState) {
  unsubscribe && unsubscribe();

  Navigation.events().registerAppLaunchedListener(() => {
    startApp(state);
  });
}

unsubscribe = store.subscribe(() => {
  const username = store.getState().user.name;
  const passProps: PassPropsType = {};
  let hasOnboarded = false;
  if (username !== null) {
    passProps.name = username;
    hasOnboarded = true;
  }
  onAppLaunch({hasOnboarded, passProps});
});
