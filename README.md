# PodMarkin

This is a demo app showcasing the use of React Native to build iOS and Android apps. This app is built using the following core tech

- React Native @ 0.63
- React Native Navigation by Wix
- Redux with persistence to store data
- Jest & Enzyme for testing

### Installation

Assuming you have installed React Native environment on your machine, follow these steps:

- Clone this repo into a suitable location
- Change into cloned directory and run `yarn`
- Once complete you can start iOS/Android app by executing `npx react-native run-ios` or `npx react-native run-android` respectively.

### App description

The purpose of this app is for the user to search and save their own collection of favourite podcasts. This app has 3 distinct screens - Welcome screen, Home screen and Saved screen.

The Welcome screen acts as a simplified authorisation screen which requires your name in order to proceed into the app.

The Home screen is like a search engine - it allows the user to query iTunes API to find the podcasts they want to save. A collection (limited to maxiumum of 25) is presented below as a scrollable list. When the user finds their podcast they can simply tap the item to save it to their saved collection.

The Saved screen is where the users saved podcasts are stored. These items are persisted and kept until the user decides to log out whereby the app clears all the data. The user can remove an item by simply tapping the list item - an alert will ask the user to confirm their action - if so, the app will remove the item from their saved collection.

### Screenshots

![Alt text](./app_screenshot.jpg?raw=true 'App screenshots')
